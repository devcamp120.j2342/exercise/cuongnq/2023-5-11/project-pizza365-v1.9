const gCOMBO_SMALL = 'Small';
const gCOMBO_MEDIUM = 'Medium';
const gCOMBO_LARGE = 'Large';

let gComBoSeclected = {};
let gPizzaSeclected = {};
let gPercent = {};

var gObjectRequest = {};

// các loại pizza

const gPIZZA_TYPE_HAI_SAN = 'haiSan';
const gPIZZA_TYPE_HAWAII = 'hawaii';
const gPIZZA_TYPE_THIT_NUONG = 'thitnuong';
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
	$('#btn-ssmail').on('click', function () {
		onBtnSmailSizeClick();
	});
	$('#btn-mmedium').on('click', function () {
		onBtnMediumSizeClick();
	});
	$('#btn-llarge').on('click', function () {
		onBtnLargeSizeClick();
	});

	$('#btn-haisan').on('click', function () {
		onBtnHaiSanClick();
	});

	$('#btn-hawai').on('click', function () {
		onBtnHawaiiClick();
	});

	$('#btn-bacon').on('click', function () {
		onBtnThitNuongClick();
	});
	onPageLoading();
	$('#btn-guidon').on('click', function () {
		onBtnGuiDonClick();
	});

	$('#btn-create-oder').on('click', function () {
		onBtnTaoDonClick();
	});
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
	onBtnGetDrinkListClick();
}
// hàm được gọi khi bấm nút chọn kích cỡ S
function onBtnSmailSizeClick() {
	'use strict';
	// gọi hàm đổi màu nút
	// chỉnh combo, combo được chọn là Small, đổi nút, và đánh dấu
	// data-is-selected-menu của nút Basic là Y, các nút khác là N
	changeComboButtonColor(gCOMBO_SMALL);
	//tạo một đối tượng menu, được tham số hóa
	gComBoSeclected = {};
	gComBoSeclected = getComboSelected('S', 20, 2, 200, 2, 150000);
	// gọi method hiển thị thông tin
	gComBoSeclected.displayInConsoleLog();
}
// hàm được gọi khi bấm nút chọn kích cỡ M
function onBtnMediumSizeClick() {
	'use strict';
	// gọi hàm đổi màu nút
	// chỉnh combo, combo được chọn là Medium, đổi nút, và đánh dấu
	// data-is-selected-menu của nút Basic là Y, các nút khác là N
	changeComboButtonColor(gCOMBO_MEDIUM);
	//tạo một đối tượng menu, được tham số hóa
	gComBoSeclected = {};
	gComBoSeclected = getComboSelected('M', 25, 4, 300, 3, 200000);
	// gọi method hiển thị thông tin
	gComBoSeclected.displayInConsoleLog();
}
// hàm được gọi khi bấm nút chọn kích cỡ L
function onBtnLargeSizeClick() {
	'use strict';
	// gọi hàm đổi màu nút
	// chỉnh combo, combo được chọn là Large, đổi nút, và đánh dấu
	// data-is-selected-menu của nút Basic là Y, các nút khác là N
	changeComboButtonColor(gCOMBO_LARGE);
	//tạo một đối tượng menu, được tham số hóa
	gComBoSeclected = {};
	gComBoSeclected = getComboSelected('L', 30, 8, 500, 4, 250000);
	// gọi method hiển thị thông tin
	gComBoSeclected.displayInConsoleLog();
}

function onBtnHawaiiClick() {
	'use strict';
	// gọi hàm đổi màu nút
	// chỉnh loại pizza, loại pizza được chọn là GÀ , đổi nút, và đánh dấu
	// data-is-selected-pizza của nút gà là Y, các nút khác là N
	changeTypeButtonColor(gPIZZA_TYPE_HAWAII);
	gPizzaSeclected = {};
	gPizzaSeclected = { pizzaType: gPIZZA_TYPE_HAWAII };
	// ghi loại pizza đã chọn ra console
	console.log('Loại pizza đã chọn: ' + gPIZZA_TYPE_HAWAII);
}
// hàm được gọi khi bấm nút chọn loại pizza Hải sản
function onBtnHaiSanClick() {
	'use strict';
	// gọi hàm đổi màu nút
	// chỉnh loại pizza, loại pizza được chọn là Hải sản, đổi nút, và đánh dấu
	// data-is-selected-pizza của nút Hải sản là Y, các nút khác là N
	changeTypeButtonColor(gPIZZA_TYPE_HAI_SAN);
	gPizzaSeclected = {};
	gPizzaSeclected = { pizzaType: gPIZZA_TYPE_HAI_SAN };
	// ghi loại pizza đã chọn ra console
	console.log('Loại pizza đã chọn: ' + gPIZZA_TYPE_HAI_SAN);
}
// hàm được gọi khi bấm nút chọn loại pizza Hun khói
function onBtnThitNuongClick() {
	'use strict';
	// gọi hàm đổi màu nút
	// chỉnh loại pizza, loại pizza được chọn là Bacon, đổi nút, và đánh dấu
	// data-is-selected-pizza của nút Bacon là Y, các nút khác là N
	changeTypeButtonColor(gPIZZA_TYPE_THIT_NUONG);
	gPizzaSeclected = {};
	gPizzaSeclected = { pizzaType: gPIZZA_TYPE_THIT_NUONG };
	// ghi loại pizza đã chọn ra console
	console.log('Loại pizza đã chọn: ' + gPIZZA_TYPE_THIT_NUONG);
}

// Hàm được gọi khi click nút gửi đơn
function onBtnGuiDonClick() {
	console.log('%c Kiểm tra đơn', 'color: orange;');
	// Bước 1: Đọc
	var vOrder = getOrder();
	console.log(vOrder);
	//Bước 2: kiểm tra
	var vKetQuaKiemTra = checkValidatedForm(vOrder);
	if (vKetQuaKiemTra) {
		//Bước 3: Hiển thị
		createOrderModal(vOrder);
		gObjectRequest = {
			kichCo: vOrder.menuCombo.menuName,
			duongKinh: vOrder.menuCombo.duongKinhCM,
			suon: vOrder.menuCombo.suongNuong,
			salad: vOrder.menuCombo.saladGr,
			loaiPizza: vOrder.loaiPizza,
			idVourcher: vOrder.voucher,
			idLoaiNuocUong: vOrder.loaiNuocUong,
			soLuongNuoc: vOrder.menuCombo.drink,
			hoTen: vOrder.hoVaTen,
			thanhTien: vOrder.priceAnnualVND(),
			email: vOrder.email,
			soDienThoai: vOrder.dienThoai,
			diaChi: vOrder.diaChi,
			loiNhan: vOrder.loiNhan,
		};
	}
}

function onBtnTaoDonClick() {
	$.ajax({
		url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
		type: 'POST',
		data: JSON.stringify(gObjectRequest),
		contentType: 'application/json;charset=UTF-8',
		dataType: 'json',
		success: function (responseObject) {
			console.log(responseObject);
			//debugger;
			handleCreateOrderSuccess(responseObject);
		},
		error: function (error) {
			console.assert(error.responseText);
		},
	});
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

function getOrder() {
	var vValueInputName = $('#inp-fullname').val().trim();

	var vValueInputEmail = $('#inp-email').val().trim();

	var vValueInputPhone = $('#inp-dien-thoai').val().trim();

	var vValueInputAddress = $('#inp-dia-chi').val().trim();

	var vValueInputMessage = $('#inp-message').val().trim();

	var vValueInputVoucherID = $('#inp-ma-giam-gia').val().trim();

	var vValueDrinkSelect = $('#sec-douong').val();
	// khái báo biến để chứa phần trăm giảm giá
	var vPercent = 0;
	// goi ham tinh phan tram
	var vVoucherObj = onBtnCheckVoucherClick(vValueInputVoucherID);
	if (vValueInputVoucherID != '' && vVoucherObj.phanTramGiamGia != undefined) {
		vPercent = vVoucherObj.phanTramGiamGia;
	}
	var vOrderInfo = {
		menuCombo: gComBoSeclected,
		loaiPizza: gPizzaSeclected.pizzaType,
		loaiNuocUong: vValueDrinkSelect,
		hoVaTen: vValueInputName,
		email: vValueInputEmail,
		dienThoai: vValueInputPhone,
		diaChi: vValueInputAddress,
		loiNhan: vValueInputMessage,
		voucher: vValueInputVoucherID,
		phanTramGiamGia: vPercent,
		priceAnnualVND: function () {
			var vTotal = this.menuCombo.priceVND * (1 - this.phanTramGiamGia / 100);
			return vTotal;
		},
	};
	return vOrderInfo;
}

function changeComboButtonColor(paramPlan) {
	var vBtnBasic = $('#btn-ssmail')[0]; // truy vấn nút chọn kích cỡ small
	var vBtnMedium = $('#btn-mmedium')[0]; //truy vấn nút chọn kích cỡ medium
	var vBtnLarge = $('#btn-llarge')[0]; //truy vấn nút chọn kích cỡ large

	if (paramPlan === gCOMBO_SMALL) {
		//nếu chọn menu Small thì thay màu nút chọn kích cỡ small bằng màu cam (btn-success), hai nút kia xanh (bg-orange)
		// đổi giá trị data-is-selected-menu
		vBtnBasic.className = 'btn btn-success w-100';
		vBtnBasic.setAttribute('data-is-selected-menu', 'Y');
		vBtnMedium.className = 'btn bg-orange w-100';
		vBtnMedium.setAttribute('data-is-selected-menu', 'N');
		vBtnLarge.className = 'btn bg-orange w-100';
		vBtnLarge.setAttribute('data-is-selected-menu', 'N');
	} else if (paramPlan === gCOMBO_MEDIUM) {
		//nếu chọn menu Medium thì thay màu nút chọn kích cỡ Medium bằng màu cam (btn-success), hai nút kia xanh (bg-orange)
		// đổi giá trị data-is-selected-menu
		vBtnBasic.className = 'btn bg-orange w-100';
		vBtnBasic.setAttribute('data-is-selected-menu', 'N');
		vBtnMedium.className = 'btn btn-success w-100';
		vBtnMedium.setAttribute('data-is-selected-menu', 'Y');
		vBtnLarge.className = 'btn bg-orange w-100';
		vBtnLarge.setAttribute('data-is-selected-menu', 'N');
	} else if (paramPlan === gCOMBO_LARGE) {
		//nếu chọn menu Large thì thay màu nút chọn kích cỡ Large bằng màu cam (btn-success), hai nút kia xanh (bg-orange)
		// đổi giá trị data-is-selected-menu
		vBtnBasic.className = 'btn bg-orange w-100';
		vBtnBasic.setAttribute('data-is-selected-menu', 'N');
		vBtnMedium.className = 'btn bg-orange w-100';
		vBtnMedium.setAttribute('data-is-selected-menu', 'N');
		vBtnLarge.className = 'btn btn-success w-100';
		vBtnLarge.setAttribute('data-is-selected-menu', 'Y');
	}
}

//function trả lại một đối tượng combo (menu được chọn) được tham số hóa
function getComboSelected(
	paramMenuName,
	paramDuongKinhCM,
	paramSuongNuong,
	paramSaladGr,
	paramDrink,
	paramPriceVND
) {
	var vSelectedMenu = {
		menuName: paramMenuName, // S, M, L
		duongKinhCM: paramDuongKinhCM,
		suongNuong: paramSuongNuong,
		saladGr: paramSaladGr,
		drink: paramDrink,
		priceVND: paramPriceVND,
		displayInConsoleLog() {
			console.log('%cPLAN MENU COMBO - .........', 'color:blue');
			console.log(this.menuName); //this = "đối tượng này"
			console.log('duongKinhCM: ' + this.duongKinhCM);
			console.log('suongNuong: ' + this.suongNuong);
			console.log('saladGr: ' + this.saladGr);
			console.log('drink:' + this.drink);
			console.log('priceVND: ' + this.priceVND);
		},
	};
	return vSelectedMenu;
}

// hàm đổi mầu nút khi chọn loại pizza
function changeTypeButtonColor(paramType) {
	var vBtnHawaii = $('#btn-hawai')[0]; // truy vấn nút chọn loại pizza gà
	var vBtnHaiSan = $('#btn-haisan')[0]; //truy vấn nút chọn loại pizza hải sản
	var vBtnThitNuong = $('#btn-bacon')[0]; //truy vấn nút chọn loại pizza bacon

	if (paramType === gPIZZA_TYPE_HAWAII) {
		//nếu chọn loại pizza Gà thì thay màu nút chọn loại pizza gà bằng màu cam (btn-success), hai nút kia xanh (bg-orange)
		// đổi giá trị data-is-selected-pizza
		vBtnHawaii.className = 'btn btn-success w-100';
		vBtnHawaii.setAttribute('data-is-selected-pizza', 'Y');
		vBtnHaiSan.className = 'btn bg-orange w-100';
		vBtnHaiSan.setAttribute('data-is-selected-pizza', 'N');
		vBtnThitNuong.className = 'btn bg-orange w-100';
		vBtnThitNuong.setAttribute('data-is-selected-pizza', 'N');
	} else if (paramType === gPIZZA_TYPE_HAI_SAN) {
		//nếu chọn loại pizza Hải sản thì thay màu nút chọn loại pizza Hải sản bằng màu cam (btn-success), hai nút kia xanh (bg-orange)
		// đổi giá trị data-is-selected-pizza
		vBtnHawaii.className = 'btn bg-orange w-100';
		vBtnHawaii.setAttribute('data-is-selected-pizza', 'N');
		vBtnHaiSan.className = 'btn btn-success w-100';
		vBtnHaiSan.setAttribute('data-is-selected-pizza', 'Y');
		vBtnThitNuong.className = 'btn bg-orange w-100';
		vBtnThitNuong.setAttribute('data-is-selected-pizza', 'N');
	} else if (paramType === gPIZZA_TYPE_THIT_NUONG) {
		//nếu chọn loại pizza Bacon thì thay màu nút chọn loại pizza Bacon bằng màu cam (btn-success), hai nút kia xanh (bg-orange)
		// đổi giá trị data-is-selected-pizza
		vBtnHawaii.className = 'btn bg-orange w-100';
		vBtnHawaii.setAttribute('data-is-selected-pizza', 'N');
		vBtnHaiSan.className = 'btn bg-orange w-100';
		vBtnHaiSan.setAttribute('data-is-selected-pizza', 'N');
		vBtnThitNuong.className = 'btn btn-success w-100';
		vBtnThitNuong.setAttribute('data-is-selected-pizza', 'Y');
	}
}

function onBtnGetDrinkListClick() {
	'use strict';
	$.ajax({
		url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',
		type: 'GET',
		dataType: 'json',
		success: function (responseObject) {
			//debugger;
			loadDataToSelect(responseObject);
		},
		error: function (error) {
			console.assert(error.responseText);
		},
	});
}

function loadDataToSelect(paramObj) {
	var vDrink = $('#sec-douong')[0];
	for (var vIndex = 0; vIndex < paramObj.length; vIndex++) {
		$('<option>', {
			value: paramObj[vIndex].maNuocUong,
			text: paramObj[vIndex].tenNuocUong,
		}).appendTo(vDrink);
	}
}

function onBtnCheckVoucherClick(paramVoucherID) {
	'use strict';
	const vBASE_URL =
		'http://203.171.20.210:8080/devcamp-pizza365/voucher_detail';
	var vVoucherId = paramVoucherID; //một số mã đúng để test: 95531, 81432,...lưu ý test cả mã sai
	if (vVoucherId === '') {
		return;
	}

	var vObj = {};

	$.ajax({
		url: vBASE_URL + '/' + vVoucherId,
		type: 'GET',
		dataType: 'json',
		async: false,
		success: function (responseObject) {
			//debugger;
			vObj = responseObject;
		},
		error: function (error) {
			alert('mã voucher ko tồn tại');
			console.assert(error.responseText);
		},
	});
	return vObj;
}

function checkValidatedForm(paramOrder) {
	if (gComBoSeclected.menuName == null) {
		alert('Chọn size ...');
		return false;
	}
	if (paramOrder.loaiPizza === undefined) {
		alert('Chọn loại pizza...');
		return false;
	}
	if (paramOrder.loaiNuocUong === '0') {
		alert('Chọn loại đồ uống ... ');
		return false;
	}
	if (paramOrder.hoVaTen === '') {
		alert('Nhập họ và tên');
		return false;
	}
	if (isEmail(paramOrder.email) == false) {
		return false;
	}

	if (paramOrder.dienThoai === '') {
		alert('Nhập vào số điện thoại...');
		return false;
	}
	if (paramOrder.diaChi === '') {
		alert('Địa chỉ không để trống...');
		return false;
	}
	return true;
}

function isEmail(paramEmail) {
	if (paramEmail < 3) {
		alert('Nhập email...');
		return false;
	}
	if (paramEmail.indexOf('@') === -1) {
		alert('Email phải có ký tự @');
		return false;
	}
	if (paramEmail.startsWith('@') === true) {
		alert('Email không bắt đầu bằng @');
		return false;
	}
	if (paramEmail.endsWith('@') === true) {
		alert('Email kết thúc bằng @');
		return false;
	}
	return true;
}

function createOrderModal(paramObj) {
	var vTienThanhToan = paramObj.priceAnnualVND();
	var vThongTin =
		'Xác nhận: ' +
		paramObj.hoVaTen +
		', ' +
		paramObj.dienThoai +
		', ' +
		paramObj.diaChi +
		'\n' +
		'Menu ' +
		paramObj.menuCombo.menuName +
		', sườn nướng ' +
		paramObj.menuCombo.suongNuong +
		', nước ' +
		paramObj.menuCombo.drink +
		'...' +
		'\n' +
		'Loại pizza: ' +
		paramObj.loaiPizza +
		', Gía: ' +
		paramObj.menuCombo.priceVND +
		'. Mã giảm giá: ' +
		paramObj.voucher +
		'\n' +
		'Phải thanh toán: ' +
		vTienThanhToan +
		'vnd (giảm giá ' +
		paramObj.phanTramGiamGia +
		'%)';

	$('#input-name').val(paramObj.hoVaTen);
	$('#input-phone').val(paramObj.dienThoai);
	$('#input-diachi').val(paramObj.diaChi);
	$('#input-loinhan').val(paramObj.loiNhan);
	$('#input-voucher').val(paramObj.voucher);
	$('#area-thongtin').val(vThongTin);
	$('#order-modal').modal('show');
}

function handleCreateOrderSuccess(paramObj) {
	alert('Tạo đơn thành công ');
	$('#order-modal').modal('hide');
	$('#thanhcong-modal').modal('show');
	$('#input-ordercode').val(paramObj.orderCode);
}
